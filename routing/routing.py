"""This module calculates directions between pairs of geographic points using Openrouteservice
and does reverse geocoding using Nominatim.
"""
import warnings
from urllib.error import URLError

from urllib3.exceptions import ConnectTimeoutError
import openrouteservice
from de4l_geodata.geodata import point as pt
from de4l_geodata.geodata import route as rt
from geopy.exc import GeocoderUnavailable, GeocoderServiceError
from geopy.geocoders import Nominatim


def reverse_geocode(route, nominatim_url, scheme='https'):
    """
    This function iterates over a route consisting of points with longitude and latitude values and converts them into
    real-world addresses using Nominatim's `reverse` function. This process is called 'reverse geocoding'. The `reverse`
    function maps coordinates to the road network by assigning them to the closest street. Nominatim is Latin for
    'by name'. It is an open source tool that can also be used to search Open Street Map (OSM) data by name and to
    generate synthetic addresses out of OSM points. We run a local instance of Nominatim. For instructions on
    deployment, take a look at the README in the repository root.


    Further resources:

    Nominatim: https://nominatim.org/

    Documentation `reverse`: https://geopy.readthedocs.io/en/stable/#geopy.geocoders.Nominatim.reverse

    Parameters
    ----------
    route : rt.Route
        A route containing points that should be reversed.
    nominatim_url : str
        The url pointing to a running instance of Nominatim.
    scheme : str
        The protocol to be used for openrouteservice queries, e.g. http or https.

    Returns
    -------
    reverse_geocoded_points : rt.Route
        A route that contains of reverse geocoded points represented as pt.Point. The points in the route have the same
        coordinate unit as the input points in the input route e.g. 'degrees'.
    failed_requests : int
        The number of requests to Nominatim that failed either because of a missing connection or because of wrong
        point coordinates.
    """
    nominatim = Nominatim(scheme=scheme, domain=nominatim_url)
    reverse_geocoded_points = rt.Route()
    failed_requests = 0
    if len(route) > 0:
        input_coordinate_unit_is_radians = route[0].get_coordinates_unit() == 'radians'
        for point in route:
            point = point.to_degrees(ignore_warning=True)
            reverse_geocoded_point = nominatim_reverse(point, nominatim)
            # Check whether a request to Nominatim succeeded. A request might fail due to connection issues or because
            # there are no results for a given point
            if isinstance(reverse_geocoded_point, pt.Point):
                if input_coordinate_unit_is_radians:
                    reverse_geocoded_point.to_radians_()
                reverse_geocoded_points.append(reverse_geocoded_point)
            else:
                failed_requests += 1

        if failed_requests > 0:
            warnings.warn(f'Reverse geocoding failed for {failed_requests} of {len(route)} points. Make sure, that the '
                          f'Nominatim service is available and serving the correct map data and that the provided '
                          f'points are in the correct format.')

    return reverse_geocoded_points, failed_requests


def nominatim_reverse(point, nominatim, zoom_level=17):
    """
    This function calls Nominatim's `reverse` function to convert a point into a real-world address. This is achieved by
    mapping locations to their closest street segment. The function returns None in the case that a connection to
    Nominatim can't be established or the provided point can not be processed by Nominatim.


    Further resources:

    Documentation reverse: https://geopy.readthedocs.io/en/stable/#geopy.geocoders.Nominatim.reverse

    Parameters
    ----------
    point : pt.Point
        The point to be reversed.
    nominatim : Nominatim
        An instance of Nominatim that is ready to accept requests.
    zoom_level : {3, 5, 8, 10, 14, 16, 17, 18}
        The level of detail required for the address reversing. Values correspond to following details levels:
            zoom 	address detail
            3 	country
            5 	state
            8 	county
            10 	city
            14 	suburb
            16 	major streets
            17 	major and minor streets
            18 	building

    Returns
    -------
    reversed_point : pt.Point or None
        In case reverse geocoding succeeds, a Point is returned. Otherwise, an error is raised. The output point has
        the same coordinates unit as the input.

    """
    reversed_location = None
    input_coordinates_unit_is_radians = point.get_coordinates_unit() == 'radians'
    # Nominatim requires latitude/longitude in 'degrees' format
    point = point.to_degrees(ignore_warnings=True)
    reversed_point = point.deep_copy()
    nominatim_input = [point.y_lat, point.x_lon]

    try:
        reversed_location = nominatim.reverse(nominatim_input, zoom=zoom_level)
    except (ValueError,
            ConnectTimeoutError,
            GeocoderUnavailable,
            ConnectionRefusedError,
            URLError,
            GeocoderServiceError) as error:
        warnings.warn(f'Failed to reverse with Nominatim, the following error occurred: {error}')

    if reversed_location is None:
        raise ValueError(f'Failed to reverse with Nominatim, the result for point {point} was empty. Please check the '
                         f'coordinates and the map data of Nominatim.')
    reversed_point.set_x_lon(reversed_location.longitude)
    reversed_point.set_y_lat(reversed_location.latitude)
    if input_coordinates_unit_is_radians:
        reversed_point.to_radians_()

    return reversed_point


def get_directions_for_points(start, end, openrouteservice_client, openrouteservice_profile='driving-car',
                              average_speed_kmh=45):
    """
    Use Openrouteservice to get directions between start and end. Directions are instructions within the traffic system
    that guide an actor towards a destination. A profile can be used to specify this actor's transport mode.

    Parameters
    ----------
    start : pt.Point
        A geographical point marking the beginning of the route to calculate.
    end : pt.Point
        A geographical point marking the end of the route to calculate.
    openrouteservice_client : openrouteservice.client.Client
        A running instance of the Openrouteservice client.
    openrouteservice_profile :
        {'driving-car', 'driving-hgv', 'foot-walking', 'foot-hiking', 'cycling-regular',
        'cycling-road', 'cycling-mountain', 'cycling-electric'}
        Specifies the mode of transport to use when calculating directions.
    average_speed_kmh : float
        The default speed in kilometers per hour, that is assumed, in case the duration of a route could not be
        retrieved and needs to be estimated. The default value is representing the average speed of a car in a city.

    Returns
    -------
    directions : dict
        Directions between start and end, containing
        distance : float
            The distance of the shortest route between start and end in meters.
        duration : float
            The duration when moving along the shortest route between start and end with the given transport mode in
            seconds.
        route : rt.Route
            The shortest route between start and end in the same format as the start point.
    """
    valid_openrouteservice_profiles = ['driving-car', 'driving-hgv', 'foot-walking', 'foot-hiking', 'cycling-regular',
                                       'cycling-road', 'cycling-mountain', 'cycling-electric']

    if not isinstance(start, pt.Point) or not isinstance(end, pt.Point):
        raise ValueError(
            'Failed to get a route for two points. Make sure the argument route contains values of type '
            'del4_geodata.geodata.point.Point.'
        )
    if not isinstance(openrouteservice_client, openrouteservice.client.Client):
        raise ValueError(
            'Failed to use the Openrouteservice client. Make sure a client for Openrouteservice was passed as argument.'
        )
    if openrouteservice_profile not in valid_openrouteservice_profiles:
        raise ValueError(
            f'The Openrouteservice profile you provided is not valid. Please choose a profile out of '
            f'{valid_openrouteservice_profiles}.'
        )

    start_coordinates_unit_is_radians = start.get_coordinates_unit() == 'radians'
    start = start.to_degrees(ignore_warnings=True)
    end = end.to_degrees(ignore_warnings=True)
    route = rt.Route([start, end])
    ors_response = None

    shortest_route = None
    distance = None
    duration = None

    try:
        ors_response = \
            openrouteservice_client.directions((start, end), profile=openrouteservice_profile, format='geojson')
    except Exception:
        # exceptions are ignored and assumed values will be provided instead (direct connection between start and end)
        pass

    if ors_response is not None:
        # ors response can contain multiple nodes (each node contains details for one shortest route proposition)
        all_nodes = ors_response['features']

        # get all nodes that have a distance (which is sometimes missing)
        nodes_with_distance = [node for node in all_nodes if 'distance' in node['properties']['summary'].keys()]

        # if multiple nodes available, choose the one with the shortest distance, if available at all
        distances = [node['properties']['summary']['distance'] for node in nodes_with_distance]
        if len(distances) > 0:
            min_distance = min(distances)
            chosen_node = \
                [node for node in nodes_with_distance if node['properties']['summary']['distance'] == min_distance][0]
        else:
            # if distance not available at all, choose the first available node and calculate the missing parameters
            chosen_node = all_nodes[0]
        shortest_route = rt.Route([
            pt.Point(point, coordinates_unit='degrees') for point in chosen_node['geometry']['coordinates']
        ])

        route_properties = chosen_node['properties']['summary']
        if 'distance' in route_properties.keys():
            distance = route_properties['distance']

        if 'duration' in route_properties.keys():
            duration = route_properties['duration']

    if shortest_route is None:
        warnings.warn(f'Shortest route could not be retrieved for {route}. The input route is returned.')
        shortest_route = route
    if distance is None:
        distance = pt.get_distance(start, end)
        warnings.warn('Distance is not available. The direct connection (as the crow flies) will be used.')
    if duration is None:
        duration = distance / (average_speed_kmh * 1_000 / 3_600)
        warnings.warn(f'Duration is not available and will be estimated based on an assumed speed of '
                      f'{average_speed_kmh} km/h.')

    if start_coordinates_unit_is_radians:
        shortest_route.to_radians_()

    return {'route': shortest_route, 'distance': distance, 'duration': duration}


def get_directions_for_route(route, openrouteservice_base_path, scheme='https', openrouteservice_profile='driving-car'):
    """
    Calculate directions between all consecutive pairs of geographical points of a route. This is done by using
    Openrouteservice and its `directions` functionality. Directions are instructions within the traffic system that
    guide an actor towards a destination. A profile can be used to specify this actor's transport mode.

    Parameters
    ----------
    route : rt.Route
        The route to get directions for.
    openrouteservice_base_path : str
        The base address of an available instance of Openrouteservice. Its structure should be '[host]:[port]'.
    scheme : str
        The protocol to be used for openrouteservice queries, e.g. http or https.
    openrouteservice_profile :
        {'driving-car', 'driving-hgv', 'foot-walking', 'foot-hiking', 'cycling-regular',
        'cycling-road', 'cycling-mountain', 'cycling-electric'}
        Specifies the mode of transport to use when calculating directions.
        See: https://openrouteservice-py.readthedocs.io/en/latest/#module-openrouteservice.directions

    Returns
    -------
    directions_for_route : List
        A list containing directions connecting every consecutive pair of points from route. Every entry is a dict
        that contains the keys 'distance', 'duration' and 'route' for the shortest route between the respective start
        and end point.
    """
    valid_openrouteservice_profiles = ['driving-car', 'driving-hgv', 'foot-walking', 'foot-hiking', 'cycling-regular',
                                       'cycling-road', 'cycling-mountain', 'cycling-electric']

    if not isinstance(route, rt.Route) or len(route) < 2:
        raise ValueError(
            'Failed to get connecting routes. Make sure to pass a route of type de4l_geodata.geodata.route.Route that '
            'contains at least two points.'
        )
    if openrouteservice_profile not in valid_openrouteservice_profiles:
        raise ValueError(
            f'The openrouteservice profile you provided is not valid. Please choose a profile out of '
            f'{valid_openrouteservice_profiles}.'
        )

    ors_url = f'{scheme}://{openrouteservice_base_path}/ors'
    client = openrouteservice.Client(base_url=ors_url)

    directions_for_route = []
    end_idx = len(route) - 1
    for idx in range(end_idx):
        start = route[idx]
        end = route[idx + 1]
        directions = get_directions_for_points(start,
                                               end,
                                               openrouteservice_client=client,
                                               openrouteservice_profile=openrouteservice_profile)
        directions_for_route.append(directions)

    return directions_for_route
