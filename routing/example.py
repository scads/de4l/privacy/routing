from geopy.geocoders import Nominatim

nominatim_url = 'localhost:8080'
nominatim = Nominatim(scheme='http', domain=nominatim_url)

expected_coords = "35.840127, -78.708416"
address = "4200-4298 Azalea Dr,Raleigh, NC 27612, USA"
params = {
    'q': address,
    'format': 'json',
    'addressdetails': 1,
    'limit': 1
}

result = nominatim.geocode(address)
print('geocoded coordinates:', result)
print('expected coordinates:', expected_coords)
