"""Test of routing.
"""

import unittest

import openrouteservice
from de4l_geodata.geodata import route as rt
from de4l_geodata.geodata import point as pt
from geopy.geocoders import Nominatim
from routing import routing


class TestMetric(unittest.TestCase):
    """Test of routing.
    """

    def test_reverse_geocode(self):
        """Test of reverse geocoding of a list of geographical points to addresses.
        """
        # todo: test reverse geocoding, once an instance of Nominatim is available from GitLab
        # nominatim_url = 'localhost:8080'
        # route = rt.Route(pt.Point([1, 1], coordinates_unit='radians'))
        #
        # reversed_route, failed_requests = routing.reverse_geocode(route, nominatim_url)
        # # Expect no failed requests
        # self.assertEqual(rt.Route, reversed_route)
        # self.assertEqual(0, failed_requests)

    def test_nominatim_reverse(self):
        """Test of reverse geocoding of a geographical point to an address.
        """
        nominatim_url = 'localhost:8080'
        nominatim = Nominatim(scheme='http', domain=nominatim_url)

        to_reverse = [(pt.Point([2.439017920469, 0.62235962758]), ValueError)]    # Tokyo Tower Tokyo, real coordinates

        # There should be value errors for every entry and different logs about the individual errors
        for point, expected_error in to_reverse:
            self.assertRaises(expected_error, routing.nominatim_reverse, point, nominatim)

    def test_get_directions_for_points(self):
        """Test of the shortest route calculation between two geographical points.
        """
        # todo: test ors services, once an instance of ORS is available from GitLab
        start_point = pt.Point([0.9, 0.1])
        end_point = pt.Point([0.8, 0.2])

        # Raise because of wrong point parameters
        for wrong_point in [[], [1.0, 1.0], '[1.0, 1.0]']:
            self.assertRaises(ValueError,
                              routing.get_directions_for_points,
                              start=wrong_point,
                              end=end_point,
                              openrouteservice_client=openrouteservice.Client(key=""))

        # Raise because of missing openrouteservice client
        self.assertRaises(ValueError,
                          routing.get_directions_for_points,
                          start=start_point,
                          end=end_point,
                          openrouteservice_client=None,
                          openrouteservice_profile='driving-car')

        # Raise because of wrong openrouteservice profile
        self.assertRaises(ValueError,
                          routing.get_directions_for_points,
                          start=start_point,
                          end=end_point,
                          openrouteservice_client=openrouteservice.Client(key=""),
                          openrouteservice_profile='rocket_spaceship')

    def test_get_directions_for_route(self):
        """Test of the shortest route calculation between each two geographical points of a route.
        """
        correct_route = rt.Route([pt.Point([-8.629335172276479, 41.15916914599747], coordinates_unit='degrees'),
                                  pt.Point([-8.659118422444477, 41.16278779752667], coordinates_unit='degrees')])
        base_path = 'localhost:8008'

        # todo: test ors services, once an instance of ORS is available from GitLab
        # Successfully calculate shortest route details
        # routing.get_directions_for_route(correct_route, base_path)

        # Raise because of an invalid route
        for wrong_route in [[], rt.Route(), rt.Route([pt.Point([0.4, 0.9])])]:
            self.assertRaises(ValueError,
                              routing.get_directions_for_route,
                              route=wrong_route,
                              openrouteservice_base_path=base_path)

        # Raise because of a wrong openrouteservice profile
        self.assertRaises(ValueError,
                          routing.get_directions_for_route,
                          route=correct_route,
                          openrouteservice_base_path=base_path,
                          openrouteservice_profile='rocket_spaceship')

        # Warn in case of a broken connection
        for _ in ['localhost:9008', 'localhos:8008', 'localhost']:
            self.assertWarns(UserWarning)
