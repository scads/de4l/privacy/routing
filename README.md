# routing
<h1>Calculate directions between geographic points using Openrouteservice and do reverse geocoding using Nominatim.</h1>

# Installation

Install this package in your repository via pip:
```bash
pip install git+ssh://git@git.informatik.uni-leipzig.de/scads/de4l/privacy/routing.git
```

## Setup

Local instances of Nominatim and Openrouteservices will be created. Further details can be found below.

All services can be started locally with

```
docker-compose up
```

Stop the services with

```
docker-compose down
```

### Nominatim

The package needs [Nominatim](https://github.com/osm-search/Nominatim) to reverse geocode coordinates.

Nominatim is set up to start as a docker container listening at port `8080`. The Nominatim service uses data about Germany.

The first start might take a while, because all files for the Nominatim service have to be generated. All following launches should be quicker, because the container persists its data in `./nominatim-data/` and will only update if new geodata is available.

### Openrouteservice (ORS)

[ORS](https://github.com/GIScience/openrouteservice) is needed to get directions between consecutive geographical points.
It listens to port `8008` of the host environment.

The ORS service is configured to calculate data for Germany. Therefore, a recent pbf-file for Germany has to be [downloaded](https://download.geofabrik.de/europe/germany-latest.osm.pbf).
The pbf then must be placed in the subdirectory `./ors-data/`

For the first build with a pbf-file, ORS has to build its graph. This might take some hours.
For every following build, `BUILD_GRAPHS` in `docker-compose.yml` can be set to false until a new pbf-file is used.